#!/usr/bin/ruby

#input = [7, 3, 6, 7, 4, 10]
input = [5,  5, 10, 40, 50, 35]
#input = [1, 2, 6, 5, 2]
#input = [5, 100, 3]
puts input.inspect

h = Hash.new(0)
t = Hash.new(0)

# Initialize impossible moves
input.length.downto(0) do |index|
  (index + 1).downto(0) do |index2|
    h[[index2, index]] = -1.0 / 0.0
  end
end

# Initialize first row
input.each_with_index do |val, index|
  h[[index + 1, 0]] = val
  t[[index + 1, 0]] = 'O'
end

1.upto(2) do |j|
  0.upto(input.length) do |i|
    next if h[[i, j]] == -1.0 / 0.0

    #puts "i, j: ", i, " ", j
#    puts "  i: #{i}"
    val = input[i - 1] + input[j - 1]
    skip = h[[i - 2, j - 2]] + input[i - 1]
    no_skip = h[[i - 2, j]] + input[i - 1]

#    puts "    val: #{val}"
#    puts "    skip: #{skip}"
#    puts "    no_skip: #{no_skip}"

    h[[i, j]] = max = [val, skip, no_skip].max

    if max == val
      t[[i, j]] = 'V'
    elsif max == skip
      t[[i, j]] = 'S'
    else
      t[[i, j]] = 'N'
    end

  end
end
#puts h.inspect
#exit

# Backtrace the step array
best = h.max_by{ |k,v| v }
puts "best score: #{best[1]}"

coord = best[0]

path = [coord[0] - 1]
# Need to figure out automatic break condition
while true do
#  puts t[coord].inspect

  if t[coord] == 'V'
    coord = [coord[0], coord[1]]
    path.push(coord[1] - 1)
    break
  elsif t[coord] == 'O'
    break
  elsif t[coord] == 'S'
    coord = [coord[0] - 2, coord[1] - 2]
  else 
    coord = [coord[0] - 2, coord[1]]
  end
  path.push(coord[0] - 1)
end

puts path.sort.inspect
